// info@stce.rwth-aachen.de
#pragma once

#include "minimizer.hpp"
#include "cppNum/linearAlgebra.hpp"

namespace co {

  template<typename T>
  class symmetricrank1_minimizer_t : public minimizer_t<T> {
      using approximation_t<T>::_states;
      using approximation_t<T>::_parameters;
      using approximation_t<T>::_accuracy;
      using approximation_t<T>::_trace;
    public:
      symmetricrank1_minimizer_t(const T& accuracy, bool trace=false);
      la::vector_t<T> run(la::vector_t<T>, const la::vector_t<T>&);
  };

}

#include "objective.hpp"
#include "cppNum/derivative.hpp"

namespace co {

    template<typename T> // constructor for the minimizer symmetric rank 1
    symmetricrank1_minimizer_t<T>::symmetricrank1_minimizer_t(const T& accuracy, bool trace) : minimizer_t<T>(accuracy, trace) {}

    template<typename T>
    la::vector_t<T> symmetricrank1_minimizer_t<T>::run(la::vector_t<T> x, const la::vector_t<T>& p) {
    	if (_trace) { _states.push_back(x); _parameters=p; } // for visualization, later on  
    	la::matrix_t<T> B = la::matrix_t<T>::Identity(x.size(), x.size()); // Initialize the Hessian with the Einheitsmatrix
	la::vector_t<T> dydx = derivative_t::dfdx<objective_t,T>(x,p); // Compute the current gradient
        
	while (dydx.norm() < _accuracy) {
		double alpha = 2;
		
		la::vector_t<T> x_prev = x;
		T y=objective_t::f(x,p), y_prev;
        	la::vector_t<T> dk = -B.lu().solve(dydx); // Finding the search direction by solving -B(k).d(k) = g(k). Use Shermann Formula for better computation time. 
        	
		// Performing Line Search
		while (y_prev<=y && alpha>_accuracy) { // backtracking line search for an approriate step size
        		la::vector_t<T> x_trial = x;
        		alpha /= 2.;
        		x_trial += alpha * dk; 
        		y = objective_t::f(x_trial,p);
      		}
		x += alpha * dk;	
	
        	la::vector_t<T> x_diff = x - x_prev;
        	la::vector_t<T> gradient_diff = derivative_t::dfdx<objective_t,T>(x,p) - dydx;
        	if (x_diff.dot(gradient_diff) > 0) {
            		B += (gradient_diff - (B * x_diff)) * (gradient_diff - (B * x_diff)).transpose() / (gradient_diff - (B * x_diff)).dot(x_diff);
        	}
		
		dydx = derivative_t::dfdx<objective_t,T>(x,p);
	} 

    	return x;
}
